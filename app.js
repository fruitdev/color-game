var squares = document.querySelectorAll('.square'),
    colors = generateRandomColors(6),
    pickedColor = pickColor(),
    colorDisplay = document.querySelector('#color-display'),
    message = document.querySelector('#message'),
    newColorsBtn = document.querySelector('#new-colors'),
    counter = 0,
    cash = '',
    gameOver = false;

colorDisplay.textContent = pickedColor;

newColorsBtn.addEventListener('click', function() {
    gameOver = false;
    counter = 0;
    colors = generateRandomColors(6);
    pickedColor = pickColor();
    colorDisplay.textContent = pickedColor;
    document.querySelector('.jumbotron').style.backgroundColor = '#50b3c8';
    message.innerHTML = '';

    drawColors();
});

function changeColors(color) {
    for (var i = 0; i < squares.length; i++) {
        squares[i].style.backgroundColor = color;
    }
    document.querySelector('.jumbotron').style.backgroundColor = color;
}

function pickColor() {
    return colors[Math.floor(Math.random() * colors.length)];
}

function generateRandomColors(lengthColors) {
    var result = [];
    for (var i = 0; i < lengthColors; i++) {
        var r = Math.floor(Math.random() * 256),
            g = Math.floor(Math.random() * 256),
            b = Math.floor(Math.random() * 256);
        result.push('rgb(' + r + ', ' + g + ', ' + b + ')');
    }

    return result;
}

for (var i = 0; i < squares.length; i++) {
    drawColors();

    squares[i].addEventListener('click', function() {
        switch (counter) {
            case 0:
                cash = '$60';
                break;
            case 1:
                cash = '$50';
                break;
            case 2:
                cash = '$40';
                break;
            case 3:
                cash = '$30';
                break;
            case 4:
                cash = '$20';
                break;
            case 5:
                cash = '$10';
                break;
            default:
                cash = 'error';
        }

        var clickedColor = this.style.backgroundColor;

        if (clickedColor === pickedColor) {
            message.innerHTML = '<h4 class="text-center alert alert-success">Correct! You win: ' + cash + '</h4>';
            changeColors(pickedColor);
            gameOver = true;
        } else {
            message.innerHTML = '<h4 class="text-center alert alert-danger">Wrong</h4>';
            this.style.backgroundColor = '#160b24';
        }

        if (!gameOver) {
            counter++;
        }
    });
}

function drawColors() {
    for (var i = 0; i < squares.length; i++) {
        squares[i].style.backgroundColor = colors[i];
    }
}