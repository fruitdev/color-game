# README #

![alt tag](http://fruit-color-game.herokuapp.com/screenshot.png)

### How to run? ###

* git clone https://fruitdev@bitbucket.org/fruitdev/color-game.git
* cd color-game
* npm i
* npm start
* Open [localhost:3000](http://localhost:3000)
* Live preview [fruit-color-game.herokuapp.com/](http://fruit-color-game.herokuapp.com/)
